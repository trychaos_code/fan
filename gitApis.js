var request = require('request'),
    async = require('async');
var chalk= require('chalk');

var arg = {};
function filesInPR(pr,cb) {
    let options = {
        method:"GET",
        url: arg.baseRepoApiUrl+'/pulls/'+pr.number+'/files',
        headers: {
            'Authorization': arg.keyStr
        }
    };
    request(options, (error, response, body) =>{
        if(!error){
            cb(body);
        }else {
            console.log("can't fetch file for pr: ",pr.number)
        }
    });
}

function CheckPrAndTakeAction(pr,cb,resPr) {
    filesInPR(pr,(body)=>{
        resPr.push({
            pr, filesInPR:JSON.parse(body)
        });
        cb(null)
    })
}
function getCommentsOnPRDet(pullreq,reviewId,callback) {
    let options = {
        method:"GET",
        url: arg.baseRepoApiUrl+'/pulls/'+pullreq.number+'/reviews/'+reviewId+'/comments',
        headers: {
            'Authorization': arg.keyStr
        },
        body:null
    };
    request(options, (error, response, body)=> {
        callback(JSON.parse(body));
    });
}

function getCommentsOnPR(pullreq,pushCb,cb) {
    let options = {
        method:"GET",
        url: arg.baseRepoApiUrl+'/pulls/'+pullreq.number+'/reviews',
        headers: {
            'Authorization': arg.keyStr
        },
        body:null
    };
    request(options, (error, response, body) =>{
        let bdy = JSON.parse(body);
        pushCb(bdy);
        cb(null);
    });
}

var myExport = {
    getAllPrs: (baseRepoApiUrl, branch, keyStr, globCb) =>{
        arg.baseRepoApiUrl = baseRepoApiUrl;
        arg.branch = branch;
        arg.keyStr = keyStr;
        let options = {
            method:"GET",
            url: baseRepoApiUrl+'/pulls?state=open',
            headers: {
                'Authorization': keyStr,
                "Accept": "application/vnd.github.black-cat-preview+json"
            }
        };
        request(options, (error, response, body) =>{
            if(!error){
                let prArr = [];
                let resPr = [];
                let prCommentId = [];
                let myBranchReq = JSON.parse(body).filter(i=>(i.base.ref === branch)).map(itmk=>({
                    id:itmk.id,
                    url:itmk.url,
                    number:itmk.number,
                    state:itmk.state,
                    title:itmk.title,
                    user:itmk.user.login,
                    userId:itmk.user.id,
                    arg:{baseRepoApiUrl, branch, keyStr}
                }));
                myBranchReq.forEach((i, ind)=>{
                    prArr.push( (cb)=> {
                        CheckPrAndTakeAction(i,cb,resPr);
                    });
                    prArr.push((cb)=>{
                        getCommentsOnPR(i, (data) =>{
                            prCommentId.push({
                                pr:i,
                                comment:data && data.length && data.map(itmk=>({
                                    id:itmk.id,
                                    user:itmk.user.login,
                                    userId:itmk.user.id,
                                }))
                            });
                        },cb);
                    });
                });
                async.series(prArr, () =>{
                    let comPrQue = [];
                    resPr.forEach((itm,ind)=>{
                        itm.filesInPR.forEach((itm1,ind1)=>{
                            comPrQue.push( (callbck) =>{
                                callbck(null);
                            })
                        });
                    });
                    async.series(comPrQue, () =>{
                        let commentContArr = [];
                        let commentCont = [];
                        prCommentId.forEach(comItm=>{
                            if(comItm.comment.length){
                                comItm.comment.forEach(comItm1=>{
                                    commentContArr.push(clbck=>{
                                        getCommentsOnPRDet(comItm.pr,comItm1.id,(comData)=>{
                                            commentCont.push(comData && comData.length && comData.map(comDataItr=>({
                                                id:comDataItr.id,
                                                url:comDataItr.url,
                                                pr_review_id:comDataItr.pull_request_review_id,
                                                diff_hunk:comDataItr.diff_hunk,
                                                path:comDataItr.path,
                                                position:comDataItr.position,
                                                original_position:comDataItr.original_position,
                                                commit_id:comDataItr.commit_id,
                                                user:comDataItr.user.login,
                                                userId:comDataItr.user.id,
                                                body:comDataItr.body,
                                                created_at:comDataItr.created_at,
                                                updated_at:comDataItr.updated_at,
                                                pr:comItm.pr
                                            })));
                                            clbck(null);
                                        })
                                    })
                                })
                            }
                        });
                        async.series(commentContArr,()=>{
                            globCb({
                                resPr,prCommentId,commentCont
                            });
                        });
                    });
                });
            }else {
                console.log("can't fetch pr")
            }
        });
    },
    commentOnIssue:(issueInFile,commentCb)=>{
        getRepositoryContents((tree)=>{
            console.log(tree)
        });
        const comment = generateComment(issueInFile);
        const posList = getPositionsForComment(issueInFile);
        var myCallBacks = [];
        posList.forEach(p => {
            if(!isCommentAlreadyPresent(issueInFile, p)){
                const prCommentObj = getCommentObj(issueInFile, comment, p);
                let options = {
                    method:"POST",
                    url: issueInFile.baseRepoApiUrlDef+'/pulls/'+issueInFile.prNumber+'/reviews',
                    headers: {
                        'Authorization': issueInFile.keyStr
                    },
                    body:JSON.stringify(prCommentObj)
                };
               
                // myCallBacks.push((nuCallBc)=>{
                //     request(options, (error, response, body) =>{
                //         nuCallBc(null);
                //     });
                // });
            }
        });
        async.series(myCallBacks,()=>{
            commentCb(null);
        });
    },
    commentOnPRGlob:()=>{},
    commentOnPR:()=>{}
};
function isCommentAlreadyPresent(issueInFile, pos){
    return false;
}
function getCommentObj(issueInFile, comment, pos){
    return {
        body:"",
        event:"COMMENT",
        comments:[{
            body: comment +" "+issueInFile.commentSuffix,
            position: pos,
            path: issueInFile.path
        }]
    };
}
function getPositionsForComment(issueInFile){
    const start = parseInt(issueInFile.patch.split(',')[0].slice(4));
    let lines = [];
    issueInFile.lines.forEach( line => {
        lines.push(parseInt(line.line));
    })
    return lines;
}

const supportedBrowserVersions = [
    {
        "name": "ie",
        "version": [
            "10",
            "9",
            "8",
            "7",
            "6"
        ]
    },
    {
        "name": "firefox",
        "version": [
            "43",
            "42",
            "41",
            "40"
        ]
    },
    {
        "name": "chrome",
        "version": [
            "48",
            "47"
        ]
    },  
    {
        "name": "edge",
        "version": [
            "13",
            "12"
        ]
    }
]

function isBrowserSupported(name, versions){
    return true;
    // let browser = supportedBrowserVersions.filter((b) => {
    //     return b.name === name;
    // });
    // browser == [] return [];
    // browser = browser[0];
    // browser.version.filter((v)=>{
    // });
}

function generateComment(issueInFile) {
    let comment =  `${issueInFile.name} not supported in `;
    issueInFile.browser.forEach( browser => {
        if(isBrowserSupported(browser.name, browser.versiona)){
            let subComment = `${browser.name} - ${browser.version[0]}, `;
            comment += subComment;
        }
    });
    return comment;
}

function getRepositoryContents(cb){
    let masterTreeSHA;
    getLatestMasterTreeSha((master)=>{
       
        masterTreeSHA = master['commit']['commit']['tree']['sha'];
        let options = {
            method:"GET",
            url: arg.baseRepoApiUrl+'/git/trees/'+masterTreeSHA+'?recursive=1',
            headers: {
                'Authorization': arg.keyStr
            }
        };
        request(options, (error, response, body) =>{
            if(!error){
                cb(JSON.parse(body));
            }else {
                console.log("can't fetch latest tree ",pr.number)
            }
        });
    });
}

function getLatestMasterTreeSha(cb){
    let options = {
        method:"GET",
        url: arg.baseRepoApiUrl+'/branches/master',
        headers: {
            'Authorization': arg.keyStr
        }
    };
    request(options, (error, response, body) =>{
        if(!error){
            cb(JSON.parse(body));
        }else {
            console.log("can't fetch latest master commit ",pr.number)
        }
    });
}
module.exports = myExport;
