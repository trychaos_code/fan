var async = require('async');
var chalk= require('chalk');
var gitApis = require('./gitApis');
var caniuseAPI = require('caniuse-api');
var sFile = {
        "html": ["tmpl", "html","jsp"],
        "css": ["css", "scss"],
        "js": ["js", "jsx"]
    },
    canIUseData = require('./lib/props.json'),
    browserList = require('./lib/default_browser_list.json');

var canIUseDataPrototype = canIUseData.js.map(i=>i.indexOf('.prototype.')!==-1?i:undefined).filter(i=>i);
canIUseData.js = canIUseData.js.filter(i=>i.indexOf('.prototype.')===-1);

function getIndicesOf(regex, str) {
    let indices=[];
    if(!regex || typeof str !== 'string' || !str) return indices;
    while (result = regex.exec(str)) {
        let newSub = str.substring(0,result.index);
        let lines = newSub.split("\n");
        indices.push({
            line:lines.length,
            wordCount:lines[lines.length-1].split(" ").length
        });
    }
    return indices;
}

function getCanIuseData(keyword, isApi, soc) {
    //var supportedInIE10 = caniuseAPI.isSupported(keyword, 'ie 10');
    var browsersObj = [];
    browserList.forEach(function (browser) {
        var browserInIteration = {
            name: browser.name,
            version: []
        };

        browser.selectedV?browser.selectedV.forEach(function (version) {
            try {
                var supported = caniuseAPI.isSupported(keyword, browser.name + " " + version);
                if (supported === false) {
                    browserInIteration.version.push(version);
                }
            }catch (e) {}
        }):'';
        browsersObj.push(browserInIteration);
    });
    return browsersObj;
}

let execFilLinRecrv = (keyword,reg, data,execFilCb)=>{
    let keywordObj = { name: keyword, browser: getCanIuseData(keyword), lines: getIndicesOf(reg,data) };
    keywordObj.browser = keywordObj.browser.filter(i=>i.version.length);
    execFilCb(keywordObj.lines.length && keywordObj.browser.length ? keywordObj : null);
};

module.exports = {
    triverceGit:function (arg,taskCB) {
        //console.log(chalk.green("arg are: ",JSON.stringify(arg,null,4)));
        gitApis.getAllPrs(arg.baseRepoApiUrlDef,arg.branch,arg.keyStr,function (prInfos) {
            //console.log(JSON.stringify(prInfos,null,4));
            let comment = [];
            let prComCbFun = (issueInFile,baseRepoApiUrlDef,number,state,keyStr,commentSuffix,filename,patch)=>{
                if(issueInFile){
                    issueInFile.baseRepoApiUrlDef = baseRepoApiUrlDef;
                    issueInFile.prNumber = number;
                    issueInFile.state = state;
                    issueInFile.keyStr = keyStr;
                    issueInFile.commentSuffix = commentSuffix;
                    issueInFile.path = filename;
                    issueInFile.patch = patch;
                    issueInFile.currentComments = prInfos.commentCont;
                    comment.push(function (commentCb) {
                        gitApis.commentOnIssue(issueInFile,commentCb)
                    })
                }
            };
            prInfos.resPr.forEach(prItms=>{
                prItms.filesInPR.forEach(filesItm=>{
                    let patch = filesItm.patch;
                    let typ = filesItm.filename.split(".");
                    let filS = typ[typ.length - 1].toLowerCase();
                    if(sFile.html.includes(filS)){
                        canIUseData.htmlAttr.forEach((keyword)=>{
                            execFilLinRecrv(keyword,new RegExp(keyword,'g'),patch, (issueInFile)=>{
                                prComCbFun(issueInFile,arg.baseRepoApiUrlDef,prItms.pr.number,prItms.pr.state,arg.keyStr,arg.commentSuffix,filesItm.filename,filesItm.patch);
                            });
                        });
                        canIUseData.html.forEach((keyword)=>{
                            execFilLinRecrv(keyword,new RegExp(keyword,'g'),patch, (issueInFile)=>{
                                prComCbFun(issueInFile,arg.baseRepoApiUrlDef,prItms.pr.number,prItms.pr.state,arg.keyStr,arg.commentSuffix,filesItm.filename,filesItm.patch);
                            });
                        });
                    }else if(sFile.css.includes(filS)){
                        canIUseData.css.forEach((keyword)=>{
                            execFilLinRecrv(keyword,new RegExp(keyword,'g'),patch, (issueInFile)=>{
                                prComCbFun(issueInFile,arg.baseRepoApiUrlDef,prItms.pr.number,prItms.pr.state,arg.keyStr,arg.commentSuffix,filesItm.filename,filesItm.patch);
                            });
                        });
                    }else if(sFile.js.includes(filS)){
                        canIUseData.js.forEach((keyword)=>{
                            execFilLinRecrv(keyword,new RegExp(keyword,'g'),patch, (issueInFile)=>{
                                prComCbFun(issueInFile,arg.baseRepoApiUrlDef,prItms.pr.number,prItms.pr.state,arg.keyStr,arg.commentSuffix,filesItm.filename,filesItm.patch);
                            });
                        });
                        canIUseDataPrototype.forEach(prp=>{
                            let cls = prp.substr(prp.indexOf('prototype.')+10);
                            let slctrPrp = new RegExp('\\.'+cls+'|\\.\\[\\\''+cls+'|\\.\\[\"'+cls,'g');
                            execFilLinRecrv(prp,slctrPrp,patch, (issueInFile)=>{
                                prComCbFun(issueInFile,arg.baseRepoApiUrlDef,prItms.pr.number,prItms.pr.state,arg.keyStr,arg.commentSuffix,filesItm.filename,filesItm.patch);
                            });
                        });
                        canIUseData.window.forEach(keyword=>{
                            execFilLinRecrv(keyword,new RegExp(keyword,'g'),patch, (issueInFile)=>{
                                prComCbFun(issueInFile,arg.baseRepoApiUrlDef,prItms.pr.number,prItms.pr.state,arg.keyStr,arg.commentSuffix,filesItm.filename,filesItm.patch);
                            });
                        });
                        canIUseData["window.console"].forEach((keyword)=>{
                            execFilLinRecrv(keyword,new RegExp(("console\\." + keyword)+"|"+("window\\.console\\." + keyword),'g'),patch, (issueInFile)=>{
                                prComCbFun(issueInFile,arg.baseRepoApiUrlDef,prItms.pr.number,prItms.pr.state,arg.keyStr,arg.commentSuffix,filesItm.filename,filesItm.patch);
                            });
                        });
                        canIUseData["window.location"].forEach((keyword)=>{
                            execFilLinRecrv(keyword,new RegExp(("location\\." + keyword)+"|"+("window\\.location\\." + keyword),'g'),patch, (issueInFile)=>{
                                prComCbFun(issueInFile,arg.baseRepoApiUrlDef,prItms.pr.number,prItms.pr.state,arg.keyStr,arg.commentSuffix,filesItm.filename,filesItm.patch);
                            });
                        });
                        canIUseData.domprop.forEach((keyword)=>{
                            execFilLinRecrv(keyword,new RegExp(keyword,'g'),patch, (issueInFile)=>{
                                prComCbFun(issueInFile,arg.baseRepoApiUrlDef,prItms.pr.number,prItms.pr.state,arg.keyStr,arg.commentSuffix,filesItm.filename,filesItm.patch);
                            });
                        });
                    }
                })
            });
            async.series(comment,()=>{
                console.log("comment:", comment.length);
                taskCB(null,"done");
            });
        })
    }
};
